package com.sid.gatewayservice.configs;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@OpenAPIDefinition(info = @Info(title = "API Gateway", version = "1.0", description = "Documentation API Gateway v1.0"))
/*@OpenAPIDefinition(
        /*servers = {
                @Server(url = "${api.server.dev.url}", description = "${api.server.dev.name}"),
                @Server(url = "${api.server.prod.url}", description = "${api.server.prod.name}")},
        info = @Info(title = "API Gateway", version = "1.0", description = "Documentation API Gateway v1.0"))*/

public class OpenApiGatewayConfig {

    @Value("${api.server.dev.name}")
    private String devUrl;
    @Value("${api.server.prod.name}")
    private String prodUrl;
    @Value("${api.title}")
    private String title;
    @Value("${api.description}")
    private String description;
    @Value("${api.version}")
    private String version;
    @Value("${api.contact.name}")
    private String contactName;
    @Value("${api.contact.url}")
    private String contactUrl;
    @Value("${api.contact.email}")
    private String contactEmail;
    @Value("${api.tos.uri}")
    private String termsOfService;
    @Value("${api.licence.key}")
    private String licenceName;
    @Value("${api.licence.url}")
    private String licenceUrl;
    @Value("${customer.params.y}")

    /*@Autowired
    RouteDefinitionLocator routeDefinitionLocator;

    @Bean
    public List<GroupedOpenApi> apis(){
        List<GroupedOpenApi> groups = new ArrayList<>();
        List<RouteDefinition> definitions = routeDefinitionLocator.getRouteDefinitions().collectList().block();
        assert definitions != null;
        definitions.stream().filter(routeDefinition -> routeDefinition.getId().matches(".*-service")).forEach(routeDefinition -> {
            String name = routeDefinition.getId().replaceAll("-service", "");
            System.out.println("hello " + name);
            groups.add(GroupedOpenApi.builder().pathsToMatch("/" + name + "/**").group(name).build());
        });
        return groups;
    }*/

    @Bean
    public OpenAPI gateWayOpenApi() {
        return new OpenAPI()
                .info(new io.swagger.v3.oas.models.info.Info()
                        .title(this.title)
                        .description(this.description)
                        .termsOfService(this.termsOfService)
                        .contact(new io.swagger.v3.oas.models.info.Contact()
                                .name(this.contactName)
                                .email(this.contactEmail)
                                .url(this.contactUrl)
                        )
                        .license(new io.swagger.v3.oas.models.info.License()
                                .name(this.licenceName)
                                .url(this.licenceUrl)
                        )
                );
    }
}
